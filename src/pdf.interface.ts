interface Pdf {
    url: string;
    fileName: string
}

export default Pdf;
