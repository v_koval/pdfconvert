import {Router, Response, Request} from 'express';
import * as fs from 'fs';
import * as path from 'path';

import * as puppeteer from 'puppeteer';

import Pdf from './pdf.interface';
import Controller from './interfaces/controller.interface';

class PdfController implements Controller {
    public path = '/';
    public router = Router();

    constructor() {
        this.initializeRoutes();
        this.initDir();
    }

    private initializeRoutes() {
        // @ts-ignore
        this.router.get(this.path, this.getPdfFile);
        this.router.post(this.path, this.convertPdf);
    }

    private initDir(){
        const inputPath = path.join(__dirname, 'public');
        const outputPath = path.join(__dirname,'converted');
        if(!fs.existsSync(inputPath)){
            fs.mkdirSync(inputPath);
        }

        if(!fs.existsSync(outputPath)){
            fs.mkdirSync(outputPath);
        }
    }

    private convertPdf(request: Request, response: Response) {
        const inputPath = path.join(__dirname, 'public');
        const outputPath = path.join(__dirname,'converted');
        const pdf: Pdf = request.body;

        const file = path.normalize(path.join(inputPath, pdf.url));
        let outfileName = path.normalize(path.join(outputPath, pdf.fileName));

        (async () => {
            const browser = await puppeteer.launch({headless: true});
            const page = await browser.newPage();
            await page.emulateMedia('print');
            await page.goto(file, {waitUntil: 'networkidle2'});
            await page.pdf({
                path: outfileName,
                displayHeaderFooter: true,
                headerTemplate:
                    `<div id="header-template" style="padding-left: 97mm; padding-top: 40px">
                        <div style="display: flex; justify-content: flex-end;">
                            <div style="font-size: 16pt; letter-spacing: .5px;">Invoice Summary</div>
                        </div>
                        <div style="display: flex; justify-content: flex-end; padding-top: 2mm;">
                            <div style="display: flex; justify-content: space-between;">
                                <div style="font-weight: 600; font-size: 6pt!important; margin-right: 10mm;">Invoice Date:</div>
                                <div style="font-size: 6pt !important;">30 Sep 2019</div>
                            </div>
                        </div>
                    </div>`,
                footerTemplate: '<div id="footer-template" style="font-size: 8pt !important; color:#808080; padding-left:120mm; text-align: right">Page <span class="pageNumber"></span> of <span class="totalPages"></span> pages</div>',
                margin: {
                    bottom: 20,
                    top: 25,
                },
                printBackground: true,
                format: 'A4'
            });
            await browser.close();
        })();
        response.statusCode = 200;
        response.end('Ok');
    }

    private getPdfFile(request: Request, response: Response){
        const outputPath = path.join(__dirname,'converted');
        const filePath = path.normalize(path.join(outputPath, request.query.name));
        fs.readFile(filePath, function (err, content) {
            if (err) throw err;

            const mime = require('mime-types').lookup(filePath);
            response.setHeader('Content-Type', mime + "; charset=utf-8");
            response.end(content);
        });
    }
}

export default PdfController;
