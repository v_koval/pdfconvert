# PDFConverter

## Build
Run `npm build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Development server

Run `npm run` for a dev server. Navigate to `http://localhost:5000/`.

## Requests
###Upload and convert html templated file.
POST: http://localhost:5000
Body:`{"url": "fileUrl", "fileName":"output_file_name.pdf"}`
 
### Download converted file
GET: http://localhost:5000/?name=output_file_name.pdf
